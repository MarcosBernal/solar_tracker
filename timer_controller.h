/*!*******************************************************************************************
 *  \file       timer_controller.h
 *  \brief      Timer controller header file.
 *  \details    This file shows the methods that allows to use timers and their documentation.
 *  \author     Marcos Bernal
 *  \author     Antonio Tabernero
 ********************************************************************************************/


#ifndef TIMERS_CONTROLLER_H
#define	TIMERS_CONTROLLER_H

#include "integer_types.h"
#include "configuration_bits.h"

/* 
 * \brief Set a number of cycles in the timer 0
 * \param cycles Number to be set in the timer
 */
void set_TMR0(uint16 cycles);

/*
 * \brief Set the initial cycles in the timer 0 to loop its interruption again(same time)
 */
void reset_TMR0(void);

/*
 * \brief Change the PIC register in order to enable the timer 0
 */
void start_TMR0(void);

/*
 * \brief Change the PIC register in order to disable the timer 0
 */
void stop_TMR0(void);

/* 
 * \brief Calculate a specific prescaler and reset timer value for a specific delay given an oscillator frequency
 * \details Computes prescaler(log2) y reset value of 16bit Timer for a period of T usec using an 
 *          oscillator of Fosc KHz  
 *          Value         0    1    2    3     4    5     6     7      8      9     ...
 *          Timer pre:   NO   1:2  1:4  1:8  1:16  1:32  1:64  1:128  1:256  1:512  ...
 *          Doesnt apply those values to timer, neither starts timer.
 *          Depending on the intended Timer the values could be invalid.
 *          For instance Timer1 and Timer3 only admits up to 1:8 prescaler.   
 * \param T number of the required delay in usec
 * \param Fosc Expected frequency in the oscillator in KHz (1 MHhz -> Fosc=1000)
 * \param prescaler OUT VALUE pointer that represents the calculated prescaler value
 * \returns The reset value to be set in the timer 0 every time that it reachs its limit
 */
uint16 get_delay(uint32 T, uint16 Fosc, uint8* prescaler);

/*
 * \brief Prepares the timer 0 in order to overflow and throw the interruption every the Specified usec
 * \param delay Number of usec to be set in the timer 0 to loop its limit and interruption
 */
void prepare_TMR0(long delay);


#endif	/* TIMERS_CONTROLLER_H */





