/*!*******************************************************************************************
 *  \file       adc_controller.h
 *  \brief      ADC controller header file.
 *  \details    This file shows the methods that allows to receive and manage the information 
 *              sended by the Photoelectric sensors.
 *  \author     Marcos Bernal
 *  \author     Antonio Tabernero
 ********************************************************************************************/


#ifndef ADC_CONTROLLER_H
#define	ADC_CONTROLLER_H

#include "integer_types.h"
#include "configuration_bits.h"
#include "serial_controller.h"
#include "timer_controller.h"

#define NUM_ADC_CHANNELS 4
#define select_ADC(ch) { ADCON0 &= 0b11000011;  ADCON0 += (ch<<2); }
#define next_adc_channel() { channel_idx = (channel_idx + 1) & (NUM_ADC_CHANNELS - 1); }


extern uint8 channel_idx;                        //!< Represents the current channel to be measured or that has been measured. Depends on the point of execution.
extern int16  last_adc_value[NUM_ADC_CHANNELS];  //!< Stores the values obtained from the photoelectric sensors.
extern uint16 common_adc_max_value_X;            //!< Represents the axis-X position that faces the light spot
extern uint16 common_adc_max_value_Y;            //!< Represents the axis-Y position that faces the light spot
extern uint8 adc_values_ready;                   //!< Used as a boolean value. It determines when the last sensor has been measured and its value has been processed.
extern uint8 photoelectric_add[];                //!< Represents the modifier to be add in the photoelectric value to balance the position that faces the light spot.

/*! 
 * \brief Initializes the ADC module.
 * \details Configures the conections in A port. 
 *          Starts the ADC peripheral and enables the ADC interruption.
 */
void ADC_init(void);


/*! 
 * \brief Closes the ADC module, cleaning the ADC_flag and disabling the ADC interruption. 
 */
void ADC_close(void);

/*! 
 * \brief Prepare the channel of the sensor to be measured.
 */
void prepare_ADC_converter(void);


/*! 
 * \brief Read the value from the photoelectric sensor.
 * \details There are 4 sensors so, the fourth reading will be the last one. 
 *          Therefore until the last reading, this void prepare the next channel.
 *          The value can be modified with the variable photoelectric_add, adding an extra value to balance the measures.
 * \returns The channel used to reading the value. (0 .. 3)
 */
uint8 read_ADC_value(void);


/*! 
 * \brief Set a value of 0 (reset) in the variables related to light spot position. 
 */
void reset_light_position(void);

/*! 
 * \brief Put the photoelectric modifier in the variable to change the original results.
 * \param photoelectric_new_mod New values that has to be set
 */
void change_photoelectric_mod(uint8 *photoelectric_new_mod);


/*! \brief Gives the max adc value depending on the uint16 passed on the arguments
 *  \param last_value the expected last highest uint16 value. if 0 then the current max value will be return
 *  \return 0 if there is no higher value
 *          !0 highest value (higher than last_value) 
 */
uint16 max_common_adc_value_X(void);


/*! \brief Gives the max adc value depending on the uint16 passed on the arguments
 *  \param last_value the expected last highest uint16 value. if 0 then the current max value will be return
 *  \return 0 if there is no higher value
 *          !0 highest value (higher than last_value) 
 */
uint16 max_common_adc_value_Y(uint16 last_value);


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////Deprecated functions (for terminal display and management)////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
 */
void notify_adc_value(void);

#endif	/* ADC_CONTROLLER_H */

