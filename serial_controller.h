/*!*******************************************************************************************
 *  \file       serial_controller.h
 *  \brief      Serial controller header file.
 *  \details    This file shows the methods that allows to use the serial connexion and 
                their documentation.
 *  \author     Marcos Bernal
 *  \author     Antonio Tabernero
 ********************************************************************************************/

#ifndef SERIALCONTROLLER_H
#define	SERIALCONTROLLER_H

#include "integer_types.h"
#include "lcd_controller.h"
#include "configuration_bits.h"

#define SYNC_MESSAGE 0xA0  //0b10100000
#define ASYN_STATE_MESSAGE 0xA2  //0b1010001X
    // States
    #define NONE_CURRENT_STATE  0x00
    #define MANUAL_MOVEM_STATE  0x01 
    #define SEARCH_LIGHT_STATE  0x02
    #define TRACK__LIGHT_STATE  0x03
    #define CLOSING__PIC_STATE  0x04

#define ASYN_VALUE_MESSAGE 0xA4  //0b101001XX
#define ASYN_ORDER_MESSAGE 0xA8  //0b10101XXX
    //Types of asyn order
    #define INITIALA_PIC   0xA8  //0b10101000
    #define WAITING_TASK   0xA9  //0b10101001
    #define PERFORM_TASK   0xAA  //0b10101010
    #define PIC_DEBUG_CH   0xAD  //0b10101101
    #define CLOSING_PIC_   0xAE  //0b10101110
    #define ERR_PIC_RECE   0xAF  //0b10101111

#define END_MESSAGE  0xFF  //0b11111111

// Length
#define SYNC_MESSAGE_LENGTH_DATA 0x12 //0b00010010 // 18
#define SYNC_MESSAGE_LENGTH (SYNC_MESSAGE_LENGTH_DATA + 3) // 3-> (start, length, stop) 21 ->(vars + meta)

#define ASYN_VALUE_LENGTH_DATA 0x16 //0b00010110 // 22
#define ASYN_VALUE_LENGTH (ASYN_VALUE_LENGTH_DATA + 3) // 3-> (start, length, stop) 21 ->(vars + meta)

#define ASYN_CODE_LENGTH_DATA 0x02 //0b00010010 // 18
#define ASYN_CODE_LENGTH (ASYN_CODE_LENGTH_DATA + 3) // 3-> (start, length, stop) 21 ->(vars + meta)

#define ASYN_STATE_LENGTH_DATA 0x05 //0b00010010 // 18
#define ASYN_STATE_LENGTH (ASYN_STATE_LENGTH_DATA + 3) // 3-> (start, length, stop) 21 ->(vars + meta)

#define END_TASK 0xFF //255

#define BUF_SIZE 128
#define inc(x) {x++; x&=(BUF_SIZE-1);}

#define BUF_SIZE_rx 32
#define inc_rx(x) {x++; x&=(BUF_SIZE_rx-1);}

extern uint8 graphic_interface_enabled; //!< (deprecated) Flag to notify if PC communication is with terminal way or graphic interface
extern uint8 new_async_message;         //!< Flag used to notify if a new async message have been loaded in the reception buffer 
extern int16 new_pos_X;                 //!< New position of axis X engine ordered by user
extern int16 new_pos_Y;                 //!< New position of axis Y engine ordered by user

/*! 
 * \brief Initialize the serial connection with the PC.
 * \details Sets the speed and configuration registers and enables the interruptions
 */
void serial_init(void);


/*! 
 * \brief   Close the serial connection with the PC.
 * \details Changes the registers in order to not send any byte.
 */
void serial_close(void);


/*! 
 * \brief Takes the byte from the USART peripheral at interruption time.
 * \details  rx_interruption gets executed when RX_flag is set(A new byte is waiting in RCREG). 
 *           Picks the byte and places it in the next available (rx_next) pos of the rx_buf (buffer), 
 *           increasing rx_next in the process. 
 */
void rx_interruption(void); 


/*! 
 * \brief Send the byte to the USART peripheral at interruption time.
 *        tx_interruption gets called when TX_flag is set(the port is ready to tramsmit).
 *        If tx_next==tx_sent, there is nothing to send and TX_INT is disable.
 *        In other case, the function sends the byte from tx_buff and increments the buffer pointer
 */
void tx_interruption(void);


/*! 
 * \brief   Counts the space available in the transmission buffer
 * \returns The space available in tx_buf 
 */
uint8 serial_space_available(void);


/*! 
 * \brief  Managed the reception buffer rx_buf to get an async message from it and gets new data from the user
 * \return The next state in which the state machine will be set 
 */
uint8 manage_received_asyn_messages(void);


/*! 
 *  \brief Sends one byte through the UART peripheral, storing it in the transmision buffer tx_buf
 *  \param ch The character or byte to be sent
 */
void send_byte(unsigned char ch);


/*! 
 *  \brief Sends one synchronous message with all sensor and important data to the graphic interface
 *  \param encoder_X Encoder position of axis X engine
 *  \param encoder_Y Encoder position of axis Y engine
 *  \param adc_values Pointer of 4 values of the adc converter
 *  \param adc_channels_number Number of adc values (4)
 *  \param pid_value_X Current value of PID in axis X engine
 *  \param pid_value_Y Current value of PID in axis Y engine

 */
void sync_message_graphic_interface(int16 encoder_X, int16 encoder_Y, int16 *adc_values, int8 adc_channels_number, uint16 pid_value_X, uint16 pid_value_Y);


/*! 
 *  \brief Sends one asynchronous STATE message with the current state in the state machine of the PIC
 *  \param state Current state in the PIC
 */
void asyn_state_notification_graphic_interface(uint8 state);


/*! 
 *  \brief Sends one asynchronous code message with the PIC condition or (PIC state regarding to the serial connection)
 *  \param code Condition to be sent
 */
void asyn_code_notification_graphic_interface(uint8 code);

/*! 
 *  \brief Sends one asynchronous message with all information about calibration
 *  \param K_values Values used in PID controller constants
 *  \param deadband_mod_X Modifier used in axis X engine in order to move just one step
 *  \param deadband_mod_Y Modifier used in axis Y engine in order to move just one step
 *  \param sign_X Spin way of the axys Y engine
 *  \param sign_Y Spin way of the axis Y engine
 *  \param photoelectric_add Modifiers added to the light sensors
 */
void asyn_valu_notification_graphic_interface(float* K_values, int16 deadband_mod_X, int16 deadband_mod_Y, int8 sign_X, int8 sign_Y, uint8* photoelectric_add);

/*! 
 *  \brief Sends one asynchronous debug message with one character or byte 
 *  \param character Byte to be debugged
 */
void asyn_debug_notification(uint8 character);


////////////////////////////////////////////////////////////////////////////////
//////////Deprecated functions (for terminal display and management)////////////
////////////////////////////////////////////////////////////////////////////////

unsigned char read_byte(void);

void send_char_vector(unsigned char *txt, uint8 size);

void send_char_pointer(unsigned char *);

void send_literal(const rom char *);

void clear_receiving_buffer(void);

/*
 *   Sends throw the serial connection strings
 *   @param type Determines the message aim it can be WARNING, ERROR, DEBUG
 *   @param txt Data to send. STRING POINTER
 *   @param txt_literal Data to send. STRING LITERAL
 *   NOTE: Use NULL to not use any data to send in txt and txt_literal
 *   NOTE: Use NULL to not use any data to send in txt and txt_literal
 */
void serial_notify_event(const rom char* type, unsigned char* txt, const rom char* txt_literal);

/*
 *  Converts an uint16 (without sign) number in its decimar character values
 *  @param number_src Number to convert
 *  @param number_dest cnversion destination
 */
void unsigned_int16_to_char(uint16 number_src, unsigned char *number_dest);

/*
 *  Converts an int16 (with sign) number in its decimar character values
 *  @param number_src Number to convert (integer with sign)
 *  @param number_dest cnversion destination
 */
void int16_to_char(int16 number_src, unsigned char *number_dest);

void get_an_integer_16bits_from_serial(uint16 *pid_constant, const rom char* var_name);

void change_3K_values_from_serial(uint16 *K1, uint16 *K2, uint16 *K3);

void serial_interface_checking(void);


#endif	/* SERIALCONTROLLER_H */

