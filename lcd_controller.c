#include "lcd_controller.h"

// Send a half of a byte(nibble) to the LCD. 
// The LCD controller works with 4 conexions.
void lcd_send_nibble(short int nibble)
{
    lcd_data(nibble); // Only the lower 4 bits of PORTD are used to send data 
    LCD_STROBE;
}


// Send a byte data to the LCD.
void lcd_putc(char c)
{
    LCD_ENABLE=0; LCD_RS=1; // Data
    lcd_send_nibble(c >> 4);
    lcd_send_nibble(c & 0xf);
    Delay10TCYx(80); //delay_us(160);
}


// Send a byte order to the LCD.
void lcd_command(short int cmd)
{
    LCD_RS=0; //Register = Instruction);
    LCD_ENABLE=0; //output_low(LCD_E);

    Delay10TCYx(30);  //delay_us(60);

    lcd_send_nibble(cmd >> 4);
    lcd_send_nibble(cmd & 0x0f);
    Delay10KTCYx(1);  //delay_ms(2);
}


void lcd_init(void)
{
    TRISD= (TRISD & 0b00000011);

    LCD_RS=0;
    LCD_ENABLE=0;

    Delay10KTCYx(8);  //delay_ms(15);
    lcd_send_nibble(0x03); Delay10KTCYx(3);  //delay_ms(5);
    lcd_send_nibble(0x03); Delay10TCYx(85);  //delay_us(170);
    lcd_send_nibble(0x03); Delay10TCYx(85);  //delay_us(170); 
    lcd_send_nibble(0x02);  // 4 bit mode


    // set interface length
    lcd_command(0x28); Delay10KTCYx(3); //delay_ms(5);
    // Enable display/Cursor
    lcd_command(0x0C); Delay10KTCYx(3); //delay_ms(5);
    // Clear Display
    lcd_command(0x01); Delay10KTCYx(3); //delay_ms(5);
    // Set Cursor Move Direction
    lcd_command(0x06); Delay10KTCYx(3); //delay_ms(5);
    // Activation of Blinking
    lcd_command(LCD_BLINK_ON); Delay10KTCYx(3); //delay_ms(5);
}


void move_cursor(short int row, short int col)
{
    short int address;

     address= (row)? 0x40:0x00;   //row=0; if (row) address = 0x40;
     address+=col;
     lcd_command( 0x80 | address);
}


void lcd_text(char *txt)
{
    char k;
    k=0; while(txt[k]) lcd_putc(txt[k++]); //for(k=0;k<n;k++) lcd_putc(txt[k]);
}


uint8 lcd_print_literal(uint8 row, const rom char *txt)
{   
    uint8 iterator=0;
    uint8 iterator2;
    
    if(row > 2 || txt == NULL)
        return WRONG_EXEC;
    
    move_cursor(row,0);
  
    while( txt[iterator] && iterator < 16) 
        lcd_putc(txt[iterator++]);    
    
    iterator2 = iterator;

    while( 16 > (iterator++)) // Remove the rest of data in the display buffer 
        lcd_putc(0x20);       // 0x20 == " "
    
    move_cursor(row,iterator2);
    
    Delay10KTCYx(5); 
    return RIGHT_EXEC;
}


