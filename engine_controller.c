#include "engine_controller.h"
#include "adc_controller.h"

uint8 activated_ADC_track = 0;
uint8 seaching_light_spot = 0;

int16 aim_encoder_X;
int16 aim_encoder_Y;
int16 rounds_encoder_X;
int16 rounds_encoder_Y;

uint16 common_adc_max_position_Y;
uint16 common_adc_max_position_X;

//Fosc -> F oscillator in KHz,   Fpwm -> desired Fpwm in KHz
// ch  -> configure channel 1 (1), 2 (2) or both (3)
// Returns max value of duty cicle
uint16 setup_PWM(uint16 Fosc,uint8 Fpwm,uint8 ch)
{
	 uint16 x, DX_max;
	 uint8 pre;
	 uint8 pr2;
	
	 x = Fosc>>1; x=(x/Fpwm)+1; x>>=1;  // Computes round((Fosc/4)/Fpwm)
	
	 if (x>16384) {pre=16; pr2=255;}    // Requested Fpwm too low -> Fpwm = Fosc/16384
	 else 
	  {
	   pre=0;
	   while(x>256) {x>>=2; pre++;}  // Find pr2 and pre such as (pr2+1)*pre = Fosc/Fpwm
	   pr2=(x-1);                    // pre 0,1,2 -> 1:1, 1:4, 1:16
	  }
	
//	 x=pr2; x++; x=x<<(2*pre); x<<=2; // 4*(pre)*(pr2+1)
//	 x = Fosc/x; Fpwm=x;   //Real Fpwm
	
	 if (ch&1) { TRISCbits.TRISC2=0; CCPR1L=0; CCP1CON = 0b00001100; } // SET channel 1
	 if (ch&2) { TRISCbits.TRISC1=0; CCPR2L=0; CCP2CON = 0b00001100; } // Set channel 2  
	
	 PR2=pr2;
	 T2CON = 0b00000100 | pre;  // start TMR2 with prescaler pre and postscaler 1:1
	  // T2CON = 0b0 1111 1 00 ;
	    //         |    | |  |
	    //         |    | |  |_ Prescale: 00 (1)  01 (4)  1X(16)
	    //         |    | |____ TIMER2 on/off (1=on, 0=off)
	    //         |    |______ PostScaler: 1:(bbbb+1) 0000: 1:1  1111: 1:16
	    //         |___________ Not used
	
     DX_max = pr2; DX_max++; DX_max<<=2;  // 4 x (PR2+1)  // MAX value for Duty cicle
	 return DX_max;
	 
}
	
void set_pwm1(uint16 duty)
{
	 CCP1CONbits.DC1B0=(duty& 0x01); duty>>=1;  //Least Significant bit
	 CCP1CONbits.DC1B1=(duty& 0x01); duty>>=1;  // 2nd Least Significant bit
	 CCPR1L=(duty);  // 8 Most Significant bits
}
	
void set_pwm2(uint16 duty)
{
	 CCP2CONbits.DC2B0=(duty& 0x01); duty>>=1;
	 CCP2CONbits.DC2B1=(duty& 0x01); duty>>=1;
	 CCPR2L=(duty);
}


int8  sign=0;
int8  sign_Y=0;
int16 deadband_mod = 0;
int16 deadband_mod_Y = 0;

float K[3]={0.0, 0.0, 0.0};

uint8 find_deadband_and_sign_X(void)
{
    uint8 deadband = 0;
    unsigned char data[5];
    aim_encoder_X = rounds_encoder_X; 
    
    start_engine_x();
    while((((aim_encoder_X-rounds_encoder_X)<5)&&((rounds_encoder_X-aim_encoder_X)<5)) && deadband < 255)
    {
       SetDCPWM2(512+(deadband++));
       Delay10KTCYx(4);
    }
    stop_engine_x();
    SetDCPWM2(512);
    
    if(aim_encoder_X!=rounds_encoder_X)
        if(aim_encoder_X>rounds_encoder_X)
            sign = -1;
        else
            sign = 1;
    
    if(graphic_interface_enabled == 1)
        asyn_valu_notification_graphic_interface(K, deadband, deadband_mod_Y, sign, sign_Y, photoelectric_add);
    else
    {
        int16_to_char(deadband, data);
        serial_notify_event("Valor de deadband:",data,NULL);
    }
    
    spin_engine_x(180);
    return deadband;
}


uint8 find_deadband_and_sign_Y(void)
{
    uint8 deadband = 0;
    unsigned char data[5];
    aim_encoder_Y = rounds_encoder_Y;
    
    start_engine_y();
    while(((aim_encoder_Y-rounds_encoder_Y)<40)&&((rounds_encoder_Y-aim_encoder_Y)<40) && deadband < 255)
    {
       SetDCPWM1(512+(deadband++));
       Delay10KTCYx(6);
    }
    stop_engine_y();
    SetDCPWM1(512);
    
    if(aim_encoder_Y!=rounds_encoder_Y)
        if(aim_encoder_Y>rounds_encoder_Y)
            sign_Y = -1;
        else
            sign_Y = 1;
    
    if(graphic_interface_enabled == 1)
        asyn_valu_notification_graphic_interface(K, deadband_mod, deadband, sign, sign_Y, photoelectric_add);
    else
    {
        int16_to_char(deadband, data);
        serial_notify_event("Valor de deadband Y:",data,NULL);
    }
    
    spin_engine_y(180);
    return deadband;
}

void engines_init(void)
{
    uint16 K_i[3] = {50, 03, 190}; 
    change_pid_constants(((float)(K_i[0]))/100, ((float)(K_i[1]))/100,((float)(K_i[2]))/100);
    
    enable_priority_levels;
    enable_high_ints; enable_low_ints;
    
    encoder_X_init();
    encoder_Y_init();
    engine_X_init();
    engine_Y_init();
    
    prepare_TMR0(10000L);
    set_TMR0_low;     //Definimos su interrupcion como BAJA prioridad 
    start_TMR0;       //Inicia timer 
}

void encoder_X_init(void)
{
   enable_INT0_int;  INT0_low2high;
   
   rounds_encoder_X=0; //~1100 encoder para hacer una vuelta
   aim_encoder_X = 0;
   
   TRISBbits.RB0 = 1; // Lectura Para el encoder
   TRISBbits.RB4 = 1; // Lectura Para el encoder
   
   serial_notify_event("DEBUG", NULL, "Inited encoder for engine X");
   serial_notify_event(NULL, NULL, "Enable Priority levels and int0(high) for encoder X");
   serial_notify_event(NULL, NULL, "Used port RB0 (B-White) and RB4(A-Yellow)");    
}

void encoder_Y_init(void)
{
   enable_INT1_int;  INT1_low2high;
   
   rounds_encoder_Y=0; //~1100 encoder para hacer una vuelta
   aim_encoder_Y = 0;
   
   TRISBbits.RB1 = 1; // Lectura Para el encoder
   TRISBbits.RB5 = 1; // Lectura Para el encoder
   
   serial_notify_event("DEBUG", NULL, "Inited encoder for engine Y");
   serial_notify_event(NULL, NULL, "Enable Priority levels and int1(high) for encoder Y");
   serial_notify_event(NULL, NULL, "Used port RB1 (B-White) and RB5(A-Yellow)");    
}

void engine_X_init(void)
{
   serial_notify_event("DEBUG", NULL, "Inited driver for engine X");
   serial_notify_event(NULL, NULL, "Enable Priority levels and TMR0 for engine X");
   serial_notify_event(NULL, NULL, "Used port RB6(Enable AB) and RC1(PWM2)"); 
   
   TRISBbits.RB6=0;
   TRISCbits.RC1=0;
   OpenPWM2(255);    // Set PWM1 and PWM2 with PR2 = 255
   OpenTimer2(TIMER_INT_OFF & T2_PS_1_1); // Starts TMR2 with 1:1 prescaler 
   
   deadband_mod = find_deadband_and_sign_X();   
}

void engine_Y_init(void)
{

   serial_notify_event("DEBUG", NULL, "Inited driver for engine X");
   serial_notify_event(NULL, NULL, "Enable Priority levels and TMR0 for engine X");
   serial_notify_event(NULL, NULL, "Used port RB7(Enable AB) and RC2(PWM1)"); 
   
   TRISBbits.RB7=0;
   TRISCbits.RC2=0;
   OpenPWM1(255);    // Set PWM1 and PWM2 with PR2 = 255
   
   deadband_mod_Y = find_deadband_and_sign_Y();
      
}

void engines_close(void)
{
    engine_X_close();
    engine_Y_close();
    encoder_X_close();
    encoder_Y_close();
    
    T0CONbits.TMR0ON=0; //Stops timer
    disable_TMR0_int; 
}

void engine_X_close(void)
{   
    spin_engine_x(180);
    Delay10KTCYx(40);
    stop_engine_x();    //Disables EngineAB
}

void engine_Y_close(void)
{   
    spin_engine_y(180);
    Delay10KTCYx(40);
    stop_engine_y();    //Disables EngineAB
}

/*
 */
void encoder_X_close(void)
{
    disable_INT0_int;
}

void encoder_Y_close(void)
{
    disable_INT1_int;
}


void encoder_X_interruption(void)
{
  	if (PORTBbits.RB4==PORTBbits.RB0) 
        rounds_encoder_X++;  
    else 
        rounds_encoder_X--;  
    INTCON2bits.INTEDG0=~INTCON2bits.INTEDG0;              // 2X encoder double resolution
    INT0_flag=0;
}

void encoder_Y_interruption(void)
{
  	if (PORTBbits.RB5==PORTBbits.RB1) 
        rounds_encoder_Y++;  
    else 
        rounds_encoder_Y--;  
    INTCON2bits.INTEDG1=~INTCON2bits.INTEDG1;              // 2X encoder double resolution
    INT1_flag=0;
}

uint16 get_degree_from_encoder(int16 encoder_counter)
{
    return ((encoder_counter/(MAX_ENCODER_COUNTER/10))*(18)+180);
}

void start_engine_x(void)
{
    PORTBbits.RB6=1;
}

void start_engine_y(void)
{
    PORTBbits.RB7=1;
}

uint8 stop_engine_x(void)
{
    PORTBbits.RB6=0;
    return RIGHT_EXEC;
}

uint8 stop_engine_y(void)
{
    PORTBbits.RB7=0;
    return RIGHT_EXEC;
}

uint8 spin_engine_x(int16 degree)
{   
    if( degree < 0 || degree > 360)
       return WRONG_EXEC;
    aim_encoder_X = (((degree/36.0)*(MAX_ENCODER_COUNTER))*2-(MAX_ENCODER_COUNTER)*10)/10; //To work with integers 360/1100 => 3.6/11
                                                                        // 1100 => Complete round => 550 half
    start_engine_x();

    return RIGHT_EXEC;
}

uint8 spin_rounds_engine_x(int16 rounds)
{   
    if( rounds < MIN_ENCODER_COUNTER || rounds > MAX_ENCODER_COUNTER)
       return WRONG_EXEC;
    aim_encoder_X = rounds; //To work with integers 360/1100 => 3.6/11
                                                                        // 1100 => Complete round => 550 half
    start_engine_x();

    return RIGHT_EXEC;
}

uint8 spin_engine_y(int16 degree)
{   
    if( degree < 0 || degree > 360)
       return WRONG_EXEC;
    aim_encoder_Y = (((degree/36.0)*(MAX_ENCODER_COUNTER))*2-(MAX_ENCODER_COUNTER)*10)/10; //To work with integers 360/1100 => 3.6/11
                                                                        // 1100 => Complete round => 550 half
    start_engine_y();

    return RIGHT_EXEC;
}

uint8 spin_rounds_engine_y(int16 rounds)
{   
    if( rounds < MIN_ENCODER_COUNTER || rounds > MAX_ENCODER_COUNTER)
       return WRONG_EXEC;
    aim_encoder_Y = rounds; //To work with integers 360/1100 => 3.6/11
                                                                        // 1100 => Complete round => 550 half
    start_engine_y();

    return RIGHT_EXEC;
}

uint16 last_pid;
void engine_X_spin_interruption(void)
{
    last_pid = compute_pid();
    
    if ( rounds_encoder_X > MAX_POS_ENCODER_X && ((last_pid > 512 && sign == 1) || (last_pid > 512 && sign == -1)))
        last_pid = 512;//last_pid_Y = (512+(deadband_mod_Y)*sign_Y);
    else if ( rounds_encoder_X < MIN_POS_ENCODER_X && ((last_pid < 512 && sign == 1) || (last_pid < 512 && sign == -1)))
        last_pid = 512;//last_pid_Y = (512-(deadband_mod_Y)*sign_Y);
    
    SetDCPWM2(last_pid);  
}

uint16 last_pid_Y;
void engine_Y_spin_interruption(void)
{
    last_pid_Y = compute_pid_Y();
        
    if ( rounds_encoder_Y < MIN_POS_ENCODER_Y &&   ((last_pid_Y < 512 && sign_Y == 1) || (last_pid_Y > 512 && sign_Y == -1)))
        last_pid_Y = 512;//last_pid_Y = (512+(deadband_mod_Y)*sign_Y);
    else if ( rounds_encoder_Y > MAX_POS_ENCODER_Y && ((last_pid_Y > 512 && sign_Y == 1) || (last_pid_Y < 512 && sign_Y == -1)))
        last_pid_Y = 512;//last_pid_Y = (512-(deadband_mod_Y)*sign_Y);
        
    SetDCPWM1(last_pid_Y);  
}

void change_pid_constants(float K1, float K2, float K3)
{
    K[0] = K1/10;
    K[1] = K2/10;
    K[2] = K3/10;

    asyn_valu_notification_graphic_interface(K, deadband_mod, deadband_mod_Y, sign, sign_Y, photoelectric_add);
}

int16 error=0;
int16 error_Y=0;
int32 error_int=0;
int32 error_int_Y=0;
int16 error_der;
int16 error_der_Y;

#define NPAST 16
int16 past[NPAST]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
int16 past_Y[NPAST]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

uint8 err_idx=0;
uint8 err_idx_Y=0;

// pid = Kp*en + Ki*sum(k=1:n => e(n-k)) + Kd(e(n) + e(n-1))
uint16 compute_pid(void)
{
 float pid;
 int16 last_error_int;
 int16 deadband_mod_temp;
 
 
    // Current error
    error=(activated_ADC_track == 0) ? aim_encoder_X-rounds_encoder_X : last_adc_value[1]-last_adc_value[0];
   
    // Derivativo error
    last_error_int = (err_idx==0) ? past[NPAST-1] : past[err_idx-1];
    error_der = (error - last_error_int);

    // Integral error
    error_int-=past[err_idx]; error_int+=error;
    past[err_idx]= error;
    err_idx++; err_idx&=(NPAST-1);
    
    
 pid = K[0]*error + K[1]*(error_int/NPAST) + K[2]*error_der;  // P,I,D terms
 
 // pid in (-1,1)
 if (pid>=1.0) pid=1.0;
 else if (pid<=-1.0) pid=-1.0;
 
 pid = (sign == -1) ? -pid  : pid;
 
 deadband_mod_temp = (pid > 0) ? deadband_mod : -(deadband_mod);
 deadband_mod_temp = (pid == 0) ? 0 : deadband_mod_temp;
 
 return ((uint16)(512+((510-deadband_mod)*pid)+deadband_mod_temp));
}

// pid = Kp*en + Ki*sum(k=1:n => e(n-k)) + Kd(e(n) + e(n-1))
uint16 compute_pid_Y(void)
{
 float pid;
 int16 last_error_int;
 int16 deadband_mod_temp;
 
 
    // Current error
    error_Y=(activated_ADC_track == 0) ? aim_encoder_Y-rounds_encoder_Y : last_adc_value[3]-last_adc_value[2];
   
    // Derivativo error
    last_error_int = (err_idx_Y==0) ? past_Y[NPAST-1] : past_Y[err_idx_Y-1];
    error_der_Y = (error_Y - last_error_int);

    // Integral error
    error_int_Y-=past_Y[err_idx_Y]; error_int_Y+=error_Y;
    past_Y[err_idx_Y]= error_Y;
    err_idx_Y++; err_idx_Y&=(NPAST-1);
    
    
 pid = K[0]*error_Y + K[1]*(error_int_Y/NPAST) + K[2]*error_der_Y;  // P,I,D terms

 // pid in (-1,1)
 if (pid>=1.0) pid=1.0;
 else if (pid<=-1.0) pid=-1.0;
 
 pid = (sign_Y == -1) ? -pid  : pid;
 
 deadband_mod_temp = (pid > 0) ? deadband_mod_Y : -(deadband_mod_Y);
 deadband_mod_temp = (pid == 0) ? 0 : deadband_mod_temp;
 
 return (512+((510-deadband_mod)*pid)+deadband_mod_temp);
}

void start_manual_engine_control(void)
{
    activated_ADC_track = 0;
    seaching_light_spot = 0;
}


void start_searching_light_spot(void)
{
    activated_ADC_track = 0;
    seaching_light_spot = 1;
    spin_engine_y(130); 
    spin_rounds_engine_x(MAX_POS_ENCODER_X); 
    reset_light_position();
}

uint16 last_max_adc_value = 0;
void searching_light_spot(void)
{
    if(seaching_light_spot == 1)
    {
        uint16 adc_value;
        if(adc_values_ready == 1)
        {
            adc_value = last_max_adc_value;
            last_max_adc_value = max_common_adc_value_X();

            common_adc_max_position_X = (last_max_adc_value > adc_value) ? rounds_encoder_X : common_adc_max_position_X;

            adc_values_ready = 0;
        }

        if(rounds_encoder_X > MAX_POS_ENCODER_X-5)
        {
            spin_rounds_engine_x(MIN_POS_ENCODER_X);
            reset_light_position();
        }
    }
    if(rounds_encoder_X < MIN_POS_ENCODER_X+5)
    {
        seaching_light_spot = 0;
        spin_rounds_engine_x(common_adc_max_position_X);
    }
}

void stop_searching_light_spot(void)
{
    seaching_light_spot = 0;
    spin_engine_x(180);
    spin_engine_y(180);
}

void start_tracking_of_light(void)
{
    spin_rounds_engine_x(common_adc_max_position_X);
    spin_rounds_engine_y(common_adc_max_position_Y);
    activated_ADC_track = 0;
    seaching_light_spot = 0;
    change_pid_constants(0.3, 0.00, 0.2);
}

void tracking_of_light(void)
{
    activated_ADC_track = 1;
}

void stop_tracking_of_light(void)
{
    activated_ADC_track = 0;
    change_pid_constants(0.5, 0.03, 0.19);
}

uint16 iterator_gra = 0;
void notify_graphic_interface(void)
{
   iterator_gra = (iterator_gra > 50)? 0 : iterator_gra++;
   
   if(iterator_gra == 0)
    sync_message_graphic_interface(rounds_encoder_X, rounds_encoder_Y,
        last_adc_value, NUM_ADC_CHANNELS, last_pid, last_pid_Y);
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////Deprecated functions (for terminal display and management)////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void get_integer_x_pid_constants(uint16 *K1, uint16 *K2, uint16 *K3)
{
    *K1 = (K[0] > 1) ? 1000 : ((uint16)(K[0]*1000));
    *K2 = (K[1] > 1) ? 1000 : ((uint16)(K[1]*1000));
    *K3 = (K[2] > 1) ? 1000 : ((uint16)(K[2]*1000));
}

void notify_rounds_encoder(void)
{
    unsigned char data[6];
    int16_to_char(rounds_encoder_X, data);
    serial_notify_event("Contador de rounds: ",data,NULL);
    int16_to_char(aim_encoder_X, data);
    serial_notify_event("Contador de aim encoder: ",data,NULL);
}

void notify_rounds_encoder_Y(void)
{
    unsigned char data[6];
    int16_to_char(rounds_encoder_Y, data);
    serial_notify_event("Contador de rounds: ",data,NULL);
    int16_to_char(aim_encoder_Y, data);
    serial_notify_event("Contador de aim encoder: ",data,NULL);
}


void test_proper_engines_movement()
{
    uint16 integer=0;
    uint8 iterations=0;
    uint16 movement_serie[9] = {70,90,150,80,300,150,300,100,250};
    uint16 movement_serie_Y[7] = {180,200,240,150,120,230,180};

    iterations = 0;
    while(iterations < 9)//361)
    {
        spin_engine_x(movement_serie[iterations++]); //spin_engine_x(50);
        Delay10KTCYx(250);
        notify_rounds_encoder();
    }
    integer = movement_serie[8];
    while(integer-- > 150)
    {
        spin_engine_x(integer);
        Delay10KTCYx(2);
    }
    iterations = 0;
    spin_engine_x(180);

                            iterations = 0;
    while(iterations < 7)//361)
    {
        spin_engine_y(movement_serie_Y[iterations++]); //spin_engine_x(50);
        Delay10KTCYx(250);
        notify_rounds_encoder_Y();
    }
    integer = movement_serie_Y[6];
    while(integer-- > 100)
    {
        spin_engine_y(integer);
        Delay10KTCYx(2);
    }
    while(integer++ < 350)
    {
        spin_engine_y(integer);
        Delay10KTCYx(2);
    }
    iterations = 0;
    spin_engine_y(180);
}

void get_maximum_sunlight_point_in_serial(void)
{
    unsigned char data[5];
    int16_to_char(common_adc_max_value_X, data);
    serial_notify_event("Valor de ADC Conjunto X",data,NULL);
    int16_to_char(get_degree_from_encoder(common_adc_max_position_X), data);
    serial_notify_event("Valor en posicion X",data,NULL);
    int16_to_char(common_adc_max_value_Y, data);
    serial_notify_event("Valor de ADC Conjunto Y",data,NULL);
    int16_to_char(get_degree_from_encoder(common_adc_max_position_Y), data);
    serial_notify_event("Valor en posicion Y",data,NULL);  
}