/*!*******************************************************************************************
 *  \file       integer_types.h
 *  \brief      Integer types definition based on their size.
 *  \details    This file defines different types of integers unsigned and signed, the files 
                also includes mandatory headers for the PIC procesor and other files in the project. 
 *  \author     Antonio Tabernero
 *  \author     Marcos Bernal
 ********************************************************************************************/

#ifndef TIPOS_H // header guard
#define TIPOS_H

#include <p18F4520.h>
#include <delays.h>

#define WRONG_EXEC 0
#define RIGHT_EXEC 1
#define NULL 0



typedef unsigned char uint8;
typedef unsigned char byte;
typedef unsigned int  uint16;
typedef unsigned long uint32;
typedef signed char int8;
typedef signed int  int16;
typedef signed long int32;

#endif /* TIPOS_H */