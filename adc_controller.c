#include <adc.h>
#include "adc_controller.h"


uint8 channel_idx = 0;
int16  last_adc_value[NUM_ADC_CHANNELS];
uint16 common_adc_max_value_X = 0;
uint16 common_adc_max_value_Y = 0;
uint8 adc_values_ready = 0;

uint8 photoelectric_add[4] = {0,0,0,0};

void ADC_init(void)
{
   TRISAbits.RA0=1; 
   TRISAbits.RA1=1;
   TRISAbits.RA2=1;
   TRISAbits.RA3=1;
      
   OpenADC(ADC_FOSC_8 & ADC_RIGHT_JUST & ADC_6_TAD,
           ADC_INT_OFF & ADC_VREFPLUS_VDD & ADC_VREFMINUS_VSS, 7);
   
   reset_light_position();
   channel_idx = 0;

   
   AD_flag=0; set_AD_low; enable_AD_int; 
   

   serial_notify_event("DEBUG", NULL, "Inited ADC for photoelectic sensor");
   serial_notify_event(NULL, NULL, "Shares TMR0 with engine X module (low priority)");
   serial_notify_event(NULL, NULL, "Used port RA0-1(V++) and GND");
   
}


void ADC_close(void)
{
   AD_flag=0; disable_AD_int; //set_AD_high;
   serial_notify_event("DEBUG", NULL, "Closing ADC module");
}


void prepare_ADC_converter(void)
{
   select_ADC(channel_idx);
   ADCON0bits.GO=1;
}


uint8 read_ADC_value(void)
{
   uint8 channel = channel_idx;
   
   last_adc_value[channel]=ADRESH; 
   last_adc_value[channel]<<=8; 
   last_adc_value[channel]+=(ADRESL&0xFC); //0xFC = 0b11111100 Remove the last two bits(noise)
   last_adc_value[channel] += photoelectric_add[channel];
   
   next_adc_channel();
   
   if(channel == NUM_ADC_CHANNELS-1)
       adc_values_ready = 1;
   else
       prepare_ADC_converter();
   
   return channel;
}


void reset_light_position(void)
{
   uint8 i;
   common_adc_max_value_X = 0;
   common_adc_max_value_Y = 0;

   adc_values_ready = 0;
   
   for(i = 0; i < NUM_ADC_CHANNELS;i++)
      last_adc_value[i] = 0;
}


void change_photoelectric_mod(uint8 * photoelectric_new_mod)
{
    photoelectric_add[0] = photoelectric_new_mod[0];
    photoelectric_add[1] = photoelectric_new_mod[1];
    photoelectric_add[2] = photoelectric_new_mod[2];
    photoelectric_add[3] = photoelectric_new_mod[3];  
}


uint16 max_common_adc_value_X(void)
{
    if(last_adc_value[1] + last_adc_value[0] > common_adc_max_value_X )
        common_adc_max_value_X = last_adc_value[1] + last_adc_value[0];

    return common_adc_max_value_X;
}


uint16 max_common_adc_value_Y(uint16 last_value)
{
    if(last_adc_value[3] + last_adc_value[2] > common_adc_max_value_Y )
        common_adc_max_value_Y = last_adc_value[3] + last_adc_value[2];
    
    return (last_value >= common_adc_max_value_Y) ? 0: common_adc_max_value_Y;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////Deprecated functions (for terminal display and management)////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


uint16 iterator = 0;
void notify_adc_value(void)
{
    int8 i = 0;
    unsigned char data[8];
    
    iterator = (iterator > 10000)? 0 : iterator++;
    
    if(iterator == 0)
    {
        data[1] = ':';
        for(i = 0; i < NUM_ADC_CHANNELS; i++)
        {
            data[0] = i;
            int16_to_char(last_adc_value[0], (&data[2]));
            serial_notify_event("ADC",data,NULL);
        }
    }
    
}