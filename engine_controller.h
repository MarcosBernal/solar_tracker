/*!*******************************************************************************************
 *  \file       engine_controller.h
 *  \brief      Engine controller header file.
 *  \details    This file shows the methods that allows use the engine through a 293L driver.
 *  \author     Marcos Bernal
 *  \author     Antonio Tabernero
 ********************************************************************************************/


#ifndef ENGINECONTROLLER_H
#define	ENGINECONTROLLER_H

#include "integer_types.h"
#include "configuration_bits.h"
#include "serial_controller.h"
#include "timer_controller.h"

#include <timers.h>  
#include <pwm.h>

#define MAX_ENCODER_COUNTER 564  // 1168 Number of encoder units to perform a round in 2X
#define MIN_ENCODER_COUNTER -564

#define MAX_POS_ENCODER_X 500
#define MIN_POS_ENCODER_X -500

#define MAX_POS_ENCODER_Y 200 
#define MIN_POS_ENCODER_Y -200

extern uint8 activated_ADC_track; //!< Determines the movement aim with photolight values
extern uint8 seaching_light_spot; //!< Determines if the program is on searching light state

extern int16 aim_encoder_X;  //!< Position where the axis-X engine should be
extern int16 aim_encoder_Y;  //!< Position where the axis-Y engine should be
extern int16 rounds_encoder_X; //!< Position of axis-X engine really is
extern int16 rounds_encoder_Y; //!< Position of axis-Y engine really is

extern uint16 common_adc_max_position_X; //!< Position of axis-X engine that faces the light spot
extern uint16 common_adc_max_position_Y; //!< Position of axis-Y engine that faces the light spot

/*! 
 * \brief Searches the deadband and the spin direction of the axys-X engine.
 * \returns The value of the deadband uint8 0 .. 255
 */
uint8 find_deadband_and_sign_X(void);

/*! 
 * \brief Searches the deadband and the spin direction of the axys-Y engine.
 * \returns The value of the deadband uint8 0 .. 255
 */
uint8 find_deadband_and_sign_Y(void);

/*! 
 * \brief Initializes the engines with the needed configuration and interruptions.
 * \details Configure engine X, engine Y, encoder X, and encoder Y. It also starts the timer 0.
 */
void engines_init(void);

/*! 
 * \brief Configures the encoder of the axys-X engine.
 */
void encoder_X_init(void);

/*! 
 * \brief Configures the encoder of the axys-Y engine.
 */
void encoder_Y_init(void);

/*! 
 * \brief Configures the axys-X engine.
 */
void engine_X_init(void);

/*! 
 * \brief Configures the engine Y.
 */
void engine_Y_init(void);

/*! 
 * \brief Closes and ends engines and encoders. It also disables the timer 0.
 * \details Closes engine X, engine Y, encoder X, and encoder Y.
 */
void engines_close(void);

/*! 
 * \brief Closes the axys-X engine.
 */
void engine_X_close(void);

/*! 
 * \brief Closes the axys-Y engine.
 */
void engine_Y_close(void);

/*! 
 * \brief Closes the encoder of the axys-Y engine.
 */
void encoder_X_close(void);

/*! 
 * \brief Closes the encoder of the axys-Y engine.
 */
void encoder_Y_close(void);


/*! 
 * \brief Manages the encoder interruption of the axys-X engine relating with the reception of a pulse.
 * \details Adds or reduces the encoder counter of the axys-X engine.
 */
void encoder_X_interruption(void);

/*! 
 * \brief Manages the encoder interruption of the axys-Y engine relating with the reception of a pulse.
 * \details Adds or reduces the encoder counter of the axys-Y engine.
 */
void encoder_Y_interruption(void);

/*! 
 * \brief Converts a encoder counter value into a degree 
 * \param encoder_counter Encoder value that have to be converted
 * \returns The degree that represents the value of engine encoder
 */
uint16 get_degree_from_encoder(int16 encoder_counter);

/*! 
 * \brief Enables the axys-X engine movement.
 * \details It activates the associate conexion with the enable of the driver L293D.
 */
void start_engine_x(void);

/*! 
 * \brief Enables the axys-Y engine movement.
 * \details It activates the associate conexion with the enable of the driver L293D.
 */
void start_engine_y(void);

/*! 
 * \brief Disables the axys-X engine movement.
 * \details It deactivates the associate conexion with the enable of the driver L293D.
 */
uint8 stop_engine_x(void);

/*! 
 * \brief Disables the axys-Y engine movement.
 * \details It deactivates the associate conexion with the enable of the driver L293D.
 */
uint8 stop_engine_y(void);

/*!
 * Moves the engine 1 (X axis) clockwise or counterclockwise.
 * \param degrees Represents the spin value of the engine. Allowed Interval  (-360, 360)
 * \return    1 if success ordering the spinning
 *            0 in other case
 */
uint8 spin_engine_x(int16 degree);

/*!
 * Moves the engine 2 (Y axis) clockwise or counterclockwise.
 * \param degrees Represents the spin value of the engine. Allowed Interval  (-360, 360)
 * \return    1 if success ordering the spinning
 *            0 in other case
 */
uint8 spin_engine_y(int16 degree);

/*! 
 * \brief Manages the interruption related to axis-X engine spining.
 * \details It uses the pid controller if the engine is not going to lose control. Uses timer 0.
 */
void engine_X_spin_interruption(void);

/*! 
 * \brief Manages the interruption related to axis-Y engine spining.
 * \details It uses the pid controller if the engine is not going to lose control. Uses timer 0.
 */
void engine_Y_spin_interruption(void);

/*! 
 * \brief Change the constant values of the PID controller
 * \details Both engines shares the PID constants.
 * \param K1 Represents the present constant
 * \param K2 Represents the past constant
 * \param K3 Represents the future constant
 */
void change_pid_constants(float K1, float K2, float K3);

// pid = Kp*en + Ki*sum(k=1:n => e(n-k)) + Kd(e(n) + e(n-1))
/*! 
 * \brief Calculates the PWM value of axis-X engine using the PID controller.
 * \details The three terms are splited. To get the integral operation is used a buffer of fix length.
 * \returns Returns the pid value that has to be applied in the engine (0 .. 1023)
 */
uint16 compute_pid(void);

/*! 
 * \brief Calculates the PWM value of axis-Y engine using the PID controller.
 * \details The three terms are splited. To get the integral operation is used a buffer of fix length.
 * \returns Returns the pid value that has to be applied in the engine (0 .. 1023)
 */
uint16 compute_pid_Y(void);

/*! 
 * \brief Prepare the manual movement state.
 * \details Performs the required task, such as, change control variables
 */
void start_manual_engine_control(void);

/*! 
 * \brief Prepare the searching light spot state.
 * \details Resets past values and order the axis-X engine going to the MAX allowed position.
 */
void start_searching_light_spot(void);

/*! 
 * \brief Performs the task of the searching light spot state.
 * \details Moves the axis-X engine from max allowed position to min allowed position seeking the spot. 
 *          Once the min allowed position has been reached, the solar tracker goes to the position where it faces the light spot. 
 */
void searching_light_spot(void);

/*! 
 * \brief Close and change the variables or configuration used just in this state.
 */
void stop_searching_light_spot(void);

/*! 
 * \brief Prepare the tracking light state.
 * \details Changes the PID contants in order to balance the movement.
 */
void start_tracking_of_light(void);

/*! 
 * \brief Performs the task of the tracking light spot state.
 * \details The main operation is made in the computer_pid function, getting the error from the light sensors.
 */
void tracking_of_light(void);

/*! 
 * \brief Close and change the variables or configuration used just in this state.
 * \details Changes the PID contants, returning the original values.
 */
void stop_tracking_of_light(void);

/*! 
 * \brief Notify the graphical interface of important variables of the microcontroller
 */
void notify_graphic_interface(void);


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////Deprecated functions (for terminal display and management)////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
 */
void get_integer_x_pid_constants(uint16 *K1, uint16 *K2, uint16 *K3);

/*
 */
void notify_rounds_encoder(void);

/*
 */
void notify_rounds_encoder_Y(void);

/*
 */
void get_maximum_sunlight_point_in_serial(void);


#endif	/* ENGINECONTROLLER_H */

