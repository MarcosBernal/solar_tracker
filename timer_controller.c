#include <timers.h>
#include "timer_controller.h"


uint16 TMR0_reset;

void set_TMR0(uint16 cycles) 
{
    TMR0H=(cycles>>8); 
    TMR0L=(cycles&0x00FF);
}

void reset_TMR0(void)
{
    TMR0H=(TMR0_reset>>8); 
    TMR0L=(TMR0_reset&0x00FF);
}

void start_TMR0(void)
{
    T0CONbits.TMR0ON=1;
}

void stop_TMR0(void)
{
    T0CONbits.TMR0ON=0;
}

uint16 get_delay(uint32 T, uint16 Fosc, uint8* prescaler)
{
  uint32 F;
  uint8 k;

  // Remove 4 usec to account for delay entering int, resetting TMR, etc
  if (T<=4) T=1; else T-=4;

  F = Fosc*T; F=F/4000;
  //F=Fosc*250; F=F*T;  // Get # machine cycles during desired period (Fosc*1000/4)*T

  // Tries to fit # machine cycles within 16 bits using prescaler
  k=0; while(F>65536L){k++; F>>=1;}
  F=65536L-F;  // Complement = Initial value of TMR so it takes T usec to overflow

  *prescaler=k;
  return (uint16)F;
}

void prepare_TMR0(long delay)
{
   uint8 PS_MASK, prescaler;
    
   enable_priority_levels; enable_TMR0_int; 
   enable_high_ints; enable_low_ints;
    
   TMR0_reset=get_delay(delay,8000,&prescaler);
   PS_MASK= (prescaler>0)?  (0xF0|(prescaler-1)): T0_PS_1_1; 
   OpenTimer0(T0_16BIT&T0_SOURCE_INT&PS_MASK);
}