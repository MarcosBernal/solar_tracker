#include "serial_controller.h"
#include "engine_controller.h"  // Change the K_PID_CONSTANTS
#include "adc_controller.h"

uint8 graphic_interface_enabled = 0;
uint8 new_async_message = 0;
int16 new_pos_X = 0;
int16 new_pos_Y = 0;

uint8 sent_tx_buf = 0;
uint8 new_tx_buf = 0;
ram char tx_buf[BUF_SIZE];   //!< Transmission buffer for USART interruption

uint8 read_rx_buf = 0;
uint8 new_rx_buf = 0; 
ram char rx_buf[BUF_SIZE_rx]; //!< Reception buffer for USART interruption

/*! 
 * \brief Calculates the required parameters to initialize the UART peripheral
 * \param spbrg Puts the value to be used as argument spbrg in OpenUSART
 * \param baud Specific rate 1200,2400,4800,9600,19200, ..., 115200
 * \param Fosc Frequency osc in KHz -> 20MHz = 20000 
 * \returns 0 (USART_BRGH_LOW)
 *          1 (USART_BRGH_HIGH)
 *          2 (cannot get speed requested)
 */
uint8 get_usart_speed(uint32 baud, uint32 Fosc, uint8* spbrg)
{
  uint16 FF;
  uint8 brgh;
  uint8 shift;

  Fosc=Fosc*1000; 
  baud>>=1; Fosc=Fosc/baud+1; Fosc>>=1;  // computes round(Fosc/baud)

  FF=(uint16)Fosc;
 
  if ((FF>16320)||(FF<16)) return 2;   // Baud rate too low or too high
 
  brgh = (FF>4096)? 0:1;  // Decides between BRGH=0 o BRGH=1
  shift= (brgh==1)? 3:5; 
  FF>>=shift; FF--; FF>>=1; // Computes round(FF/64 -1) or round(FF/16-1)

  *spbrg = (uint8)FF;
  return brgh;
}


/*! 
 * \brief Set up the UART peripheral registers
 * \param brgh Determines if uses high or low speed (0/1)
 * \param spbrg Parameter to specify the speed calculated in get_usart_speed 
 */
void setup_UART(uint8 brgh,uint8 spbrg)  // 9600 @ 20 MHz  -> 1,129
{
    //TRISC.RC6=0; TRISC.RC7=1;  // RC6 out (tx), RC7 in (rx)
    TRISCbits.TRISC6=0; TRISCbits.TRISC7=1;  // RC6 out (tx), RC7 in (rx)

    TXSTA =  0b00100110; //bits 0b_76543210
    /* TXSTA = 0x00;  // Clear TX status
     * TXSTA.TXEN=1;  // Transmit Enabled  ==> bit 5
     * TXSTA.SYNC=0;  // Async mode ==> bit 4
     * TXSTA.BRGH=1;  // High Speed (asynch) ==> bit 2
     * TXSTA.TRMT=1;  // TRS empty  (we start with an empty TX register) ==> bit 1
     */

    RCSTA = 0b10010000; //bits 0b_76543210
    /* RCSTA = 0x00;  // Clear RC status
     * RCSTA.SPEN=1;  // Enable Serial Port ==> bit 7
     * RCSTA.CREN=1;  // Enable Reciever ==> bit 4
     */

    TXSTAbits.BRGH=brgh;  // high/low speed (determines baudrate along with SPBRFG)
   // BRGH=0 -> Baud Rate = Fosc / (64 * (SPBRG+1))
   // BRGH=1 -> Baud Rate = Fosc / (16 * (SPBRG+1))
    SPBRG = spbrg;  
}


void serial_init(void)
{
    uint8 brgh;
    uint8 sp;

    brgh=get_usart_speed(2400,8000,&sp);
    setup_UART(brgh,sp);

    sent_tx_buf = 0; new_tx_buf = 0;
    
    read_rx_buf = 0; new_rx_buf = 0;
    
    graphic_interface_enabled = 1;
    //serial_interface_checking(); //(deprecated)
    
    enable_priority_levels;
    set_RX_low;
    set_TX_low;

    
    enable_RX_int; enable_global_ints; enable_perif_ints;
    
    lcd_print_literal(LCD_SECOND_ROW, "SE: Ok Created"); //Add enough delay to remove the old one
    
    lcd_print_literal(LCD_SECOND_ROW, "SE: InterfaceGra");

    asyn_code_notification_graphic_interface(INITIALA_PIC);
}


void serial_close(void)  // 9600 @ 20 MHz  -> 1,129
{
    asyn_code_notification_graphic_interface(CLOSING_PIC_);
    
    lcd_print_literal(LCD_SECOND_ROW, "SE: Closed!!"); //Add enough delay to remove the old one
    Delay10KTCYx(5);
    
    TXSTA = 0x00;
    RCSTA = 0x00;  
}


void rx_interruption(void)
{
    uint8 byte = RCREG;
    
    if(graphic_interface_enabled == 1)
    {
        byte = (byte==250) ? 0: byte;
        if(new_rx_buf > 2 && ( rx_buf[1]+2 <= new_rx_buf)) // 2 because the add op is below
            new_async_message = 1;
    }
    
    rx_buf[new_rx_buf] = byte;  // Read RX char and place it in the RX buffer
    inc_rx(new_rx_buf); 
    RX_flag=0;
}


void tx_interruption(void)
{
    if (sent_tx_buf==new_tx_buf)   
        disable_TX_int; 
    else
    {
        TXREG=tx_buf[sent_tx_buf]; 
        inc(sent_tx_buf); 
    }
    TX_flag=0;
}


uint8 serial_space_available(void)
{
    if( new_tx_buf == sent_tx_buf )
        return BUF_SIZE;
    else
        if( new_tx_buf > sent_tx_buf )
            return (BUF_SIZE-new_tx_buf)+sent_tx_buf;
        else
            return sent_tx_buf-new_tx_buf;
}


uint8 manage_received_asyn_messages()
{
  uint8 state = NONE_CURRENT_STATE;  
  float * k1;
  float * k2;
  float * k3;
  uint8 photoelectric_rec[4];
    
      if(rx_buf[rx_buf[1]+2] != END_MESSAGE)
          lcd_print_literal(LCD_SECOND_ROW, "SE: ErrPICAsyn");
      
      switch(rx_buf[0])
      {
          case(ASYN_VALUE_MESSAGE):
              k1 = ((float *)&rx_buf[2]);
              k2 = ((float *)&rx_buf[6]);
              k3 = ((float *)&rx_buf[10]);
              photoelectric_rec[0] = ((uint8)rx_buf[20]); 
              photoelectric_rec[1] = ((uint8)rx_buf[21]); 
              photoelectric_rec[2] = ((uint8)rx_buf[22]); 
              photoelectric_rec[3] = ((uint8)rx_buf[23]); 
              change_photoelectric_mod(photoelectric_rec);
              change_pid_constants(*k1, *k2, *k3);
              asyn_code_notification_graphic_interface(WAITING_TASK);
              break;
          case(ASYN_STATE_MESSAGE):
              state = rx_buf[2];
              if(state == MANUAL_MOVEM_STATE)
              {
                  new_pos_X = *((int16 *)&rx_buf[3]);
                  new_pos_Y = *((int16 *)&rx_buf[5]);
              }
              asyn_code_notification_graphic_interface(WAITING_TASK);
              break;
          default:
              asyn_code_notification_graphic_interface(ERR_PIC_RECE);
      }
      
      new_rx_buf = 0;
      new_async_message = 0;
      return state;
}


void send_byte(unsigned char ch)
{
    if (((new_tx_buf+1)&BUF_SIZE-1)==sent_tx_buf)   // If number of char overflow 
    {
        enable_TX_int;                              // The transmision is enable again
            Delay1KTCYx(5);
        disable_TX_int;
    }
    
    if(graphic_interface_enabled == 1 && (ch == 0 || ch == 250)) // absolute 0 can not be sent
        ch = (ch == 0) ? 250 : 251;                              // 0 is represented as 250
    
    tx_buf[new_tx_buf] = ch;
    inc(new_tx_buf);
}


void sync_message_graphic_interface(int16 encoder_X, int16 encoder_Y, int16 adc_values[], int8 adc_channels_number, uint16 pid_value_X, uint16 pid_value_Y)
{
    uint8 i,j;
    unsigned char vars_data[SYNC_MESSAGE_LENGTH_DATA];
    
    if(graphic_interface_enabled == 0 || serial_space_available() < SYNC_MESSAGE_LENGTH) 
        return;                       // It must not be delay in interruptions
    
    for(i = 0; i < SYNC_MESSAGE_LENGTH_DATA; i++) 
        vars_data[i] = 0x00;//0x00;
      
    vars_data[0] = encoder_X & 0xFF;
    vars_data[1] = ((encoder_X >> 8) & 0xFF);

    vars_data[2] = encoder_Y & 0xFF;
    vars_data[3] = (encoder_Y >> 8)& 0xFF;

    for(j = 0; j < adc_channels_number; j++) // 4-5,6-7,8-9,10-11
    {
        vars_data[4 + j*2] = adc_values[j] & 0xFF;
        vars_data[5 + j*2] = (adc_values[j] >> 8) & 0xFF;                   
    }

    vars_data[12] = adc_channels_number & 0xFF;
    //vars_data[13] Reserved

    vars_data[14] = pid_value_X & 0xFF;
    vars_data[15] = (pid_value_X >> 8) & 0xFF;

    vars_data[16] = pid_value_Y & 0xFF;
    vars_data[17] = (pid_value_Y >> 8)& 0xFF;
    
    send_byte(SYNC_MESSAGE);
    send_byte(SYNC_MESSAGE_LENGTH_DATA);
    send_char_vector(vars_data, SYNC_MESSAGE_LENGTH_DATA);
    send_byte(END_MESSAGE);

}


void asyn_state_notification_graphic_interface(uint8 state)
{
    if(graphic_interface_enabled == 0 || serial_space_available() < ASYN_STATE_LENGTH) 
        return;   

    send_byte(ASYN_STATE_MESSAGE);
    send_byte(ASYN_STATE_LENGTH_DATA);
    send_byte(state);
    send_byte(0x00); //posX in interface
    send_byte(0x00); //posX in interface
    send_byte(0x00); //posY in interface
    send_byte(0x00); //posY in interface
    send_byte(END_MESSAGE);
}


void asyn_code_notification_graphic_interface(uint8 order)
{
    if(graphic_interface_enabled == 0 || serial_space_available() < ASYN_CODE_LENGTH) 
        return;   

    if(order < 0xA8 || order > 0xAF)
        return;
    
    send_byte(ASYN_ORDER_MESSAGE);
    send_byte(ASYN_CODE_LENGTH_DATA);
    send_byte(order);
    send_byte(0x00); //Reserved for reply
    send_byte(END_MESSAGE);
}


void asyn_valu_notification_graphic_interface(float* K_values, int16 deadband_mod_X, int16 deadband_mod_Y, int8 sign_X, int8 sign_Y, uint8* photoelectric_mod)
{
    uint8 i;
    unsigned char vars_data[ASYN_VALUE_LENGTH_DATA];
    
    if(graphic_interface_enabled == 0 || serial_space_available() < ASYN_VALUE_LENGTH) 
        return;   
 
    for(i = 0; i < ASYN_VALUE_LENGTH_DATA; i++) 
        vars_data[i] = 0x00;//0x00;
     
    vars_data[0] = ((uint32*)K_values)[0] & 0xFF;
    vars_data[1] = (((uint32*)K_values)[0] >> 8) & 0xFF;
    vars_data[2] = (((uint32*)K_values)[0] >> 16) & 0xFF;
    vars_data[3] = (((uint32*)K_values)[0] >> 24) & 0xFF;
    
    vars_data[4] = ((uint32*)K_values)[1] & 0xFF;
    vars_data[5] = (((uint32*)K_values)[1] >> 8) & 0xFF;
    vars_data[6] = (((uint32*)K_values)[1] >> 16) & 0xFF;
    vars_data[7] = (((uint32*)K_values)[1] >> 24) & 0xFF;
    
    vars_data[8]  = ((uint32*)K_values)[2] & 0xFF;
    vars_data[9]  = (((uint32*)K_values)[2] >> 8) & 0xFF;
    vars_data[10] = (((uint32*)K_values)[2] >> 16) & 0xFF;
    vars_data[11] = (((uint32*)K_values)[2] >> 24) & 0xFF;
    
    vars_data[12] = deadband_mod_X & 0xFF;
    vars_data[13] = (deadband_mod_X >> 8) & 0xFF;
    
    vars_data[14] = deadband_mod_Y & 0xFF;
    vars_data[15] = (deadband_mod_Y >> 8) & 0xFF;
    
    vars_data[16] = sign_X & 0xFF;
    vars_data[17] = sign_Y & 0xFF;
    
    vars_data[18] = photoelectric_mod[0] & 0xFF;
    vars_data[19] = photoelectric_mod[1] & 0xFF;
    vars_data[20] = photoelectric_mod[2] & 0xFF;
    vars_data[21] = photoelectric_mod[3] & 0xFF;
    
    send_byte(ASYN_VALUE_MESSAGE);
    send_byte(ASYN_VALUE_LENGTH_DATA);
    send_char_vector(vars_data, ASYN_VALUE_LENGTH_DATA);
    send_byte(END_MESSAGE);
}

void asyn_debug_notification(uint8 character)
{
    if(graphic_interface_enabled == 0 || serial_space_available() < ASYN_CODE_LENGTH) 
        return;   

    send_byte(ASYN_ORDER_MESSAGE);
    send_byte(ASYN_CODE_LENGTH_DATA);
    send_byte(PIC_DEBUG_CH);
    send_byte(character); //Reserved for reply
    send_byte(END_MESSAGE);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////Deprecated functions (for terminal display and management)////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void new_line(void){
    send_byte(0x0D); //RETORNO DE CARRO
    send_byte(0x0A); //SALTO DE LINEA
}


void end_message(void){
    send_byte(0x00); //NULL ASCII CODE
}


/* Reads one byte through the UART peripheral
 * @param ch The character or byte to be read
 */
unsigned char read_byte(void)
{
    unsigned char ch = '.';
    if (read_rx_buf!=new_rx_buf)
    {
        ch= rx_buf[read_rx_buf];
        inc_rx(read_rx_buf);
    }
    return ch;
}


void send_char_vector(unsigned char *txt, uint8 size){
    uint8 iterator;
    
    disable_TX_int;
    for(iterator = 0; iterator < size; iterator++)
        send_byte(txt[iterator]);
    enable_TX_int;
}


/*
 * Sends a string POINTER throught the serial channel. PRE: the string has to end in '\0' 
 * or has a length lower than 64 characters.
 * @param String to send 
 */
/* NOTES: Use of const rom char* due to MPLAB18 working. PG106 FAQ-5 51295 Getting Started
 */ //NOTES
void send_char_pointer(unsigned char *txt){
    uint8 len=0;
    uint8 full_len=0;
    disable_TX_int;
    for (full_len = 0; txt[full_len] != '\0' && full_len < 64; full_len++);
    while(len < full_len)
        send_byte(txt[len++]);
    enable_TX_int;
}


/*
 * Sends a string LITERAL throught the serial channel. PRE: the string has to end in '\0' 
 * or has a length lower than 64 characters.
 * @param String to send 
 */
/* NOTES: Use of const rom char* due to MPLAB18 working. PG106 FAQ-5 51295 Getting Started
 */ //NOTES
void send_literal(const rom char *txt){
    uint8 len=0;
    uint8 full_len=0;

    for (full_len = 0; txt[full_len] != '\0' && full_len < 64; full_len++);

    disable_TX_int;
        while(len < full_len)
            send_byte(txt[len++]);
    enable_TX_int;
}


void clear_receiving_buffer(void)
{
    while(read_rx_buf!=new_rx_buf) read_byte();
}


void serial_notify_event(const rom char* type, unsigned char* txt, const rom char* txt_literal)
{
    if(graphic_interface_enabled == 1)
        return;
        
    if(type != NULL)
    {
        send_literal("TYPE: ");
        send_literal(type);
        send_literal(" -> ");
    }
    if(txt_literal != NULL && txt != NULL)
    {
        send_char_pointer(txt);  new_line();
        send_literal(txt_literal);  
    }
    else if(txt_literal != NULL)
    {
        send_literal(txt_literal);
    }
    else if(txt != NULL)
    {
        send_char_pointer(txt);
    }
    new_line();
    end_message();
}


void unsigned_int16_to_char(uint16 number_src, unsigned char *number_dest)
{
    uint8 len = 0;
    uint8 iterator = 0;
    char number_data[10];
    
    do
      number_data[len++] = ((uint8) 48 + (number_src - ((uint8) (number_src/10))*10));
    while((number_src/=10) >= 10);
       
    number_data[len] = ((uint8) 48 + number_src);
    number_data[len+1] = ((uint8) 0x00);
    
    do
      number_dest[iterator] = number_data[len-iterator];
    while(iterator++ < len);
    
    number_dest[len+1] = 0x00;
}


void int16_to_char(int16 number_src, unsigned char *number_dest)
{
    if(number_src & 0x8000) // The first bit has the sign 8 -> 8 = 0b1000
    {
        unsigned_int16_to_char((~number_src)+1, &number_dest[1]);
        number_dest[0] = ((char) '-');
    }
    else
        unsigned_int16_to_char(number_src, number_dest);
}


uint8 serial_task = 0;
uint8 menu_serial(void)
{
    unsigned char prueba;
    uint8 interactive_value = 0;
    
    
    if(graphic_interface_enabled == 1)
    {
        asyn_code_notification_graphic_interface(WAITING_TASK);
        
        while(serial_task==0) Delay10KTCYx(50);
        
        asyn_code_notification_graphic_interface(PERFORM_TASK);
        
        interactive_value = serial_task;
        serial_task = 0;
    }
    else
    {
        if (read_rx_buf==new_rx_buf)
        {
          serial_notify_event(NULL,NULL,"Inserte una nueva tarea.");
          serial_notify_event(NULL,NULL,"Opciones: '1' - Engine X(AUTO),   '2' - ADC X(Light Tracking)");
          serial_notify_event(NULL,NULL,"          '3' - Change K(pid),  '4' - Engine(Manual)");
          serial_notify_event(NULL,NULL,"          '5' - Engine Y(AUTO),   '6' - ADC Y(Light Tracking)");
          serial_notify_event(NULL,NULL,"          '7' - EXIT");
        }

        while(read_rx_buf==new_rx_buf) Delay10KTCYx(50);

        prueba = read_byte();

        if((prueba >= 0x30 && prueba <= 0x39)) // 0x30 -> 0 ... 0x39 -> 9 ... 
            interactive_value = prueba - 0x30;                  // 0x2E -> '.'
        else
            interactive_value = menu_serial();
        
        interactive_value = (interactive_value == 7) ? END_TASK : interactive_value;
        
    }
    return interactive_value;
}


void get_an_integer_16bits_from_serial(uint16 *pid_constant, const rom char* var_name)
{
    unsigned char digit = '0';
    uint16 constant = 65535;
    unsigned char data[6];
    uint8 digit_position = 1;
    while(constant > 65534)
    {
        constant = 0;
        digit_position = 1;
        digit = '0';
        serial_notify_event("Inserte ",NULL,var_name);
        while(digit != ';' & (digit_position++ <= 5))
        {
            while(read_rx_buf==new_rx_buf) Delay10KTCYx(1);
            digit = read_byte();
            constant = (48 <= digit && digit <= 57) ? ((constant*10)+ (digit-48)) : constant;        
        }
    }
    
    int16_to_char(constant, data);
    serial_notify_event(NULL,data,NULL);
    *pid_constant = constant;

}


void change_3K_values_from_serial(uint16 *K1, uint16 *K2, uint16 *K3)
{
    uint16 K[3] = {0,0,0};
    
    serial_notify_event(NULL,NULL,"Valores entre 100 y 1 (1.00 - 0.01). Termine con ';'");
       
    get_an_integer_16bits_from_serial(&(K[0]),"Inserte K1");
    *K1 = (K[0] > 1000 || K[0] < 0) ? *K1 : ((uint16)K[0]); 
    
    get_an_integer_16bits_from_serial(&(K[1]),"Inserte K2");
    *K2 = (K[1] > 1000 || K[1] < 0) ? *K2 : ((uint16)K[1]);
    
    get_an_integer_16bits_from_serial(&(K[2]),"Inserte K3");
    *K3 = (K[2] > 1000 || K[2] < 0) ? *K3 : ((uint16)K[2]);
    
}


void serial_interface_checking(void)
{
    uint8 brgh, sp;
    uint8 iterator = 0;
    unsigned char data[5];
    
    send_literal("---");
    
    Delay10KTCYx(150);
    Delay10KTCYx(150);
    Delay10KTCYx(150);
    
    while(read_rx_buf!=new_rx_buf && iterator < 5) 
        data[iterator++] = read_byte();
    
    if(iterator != 5 && data[0] == '1' && data[1] == '2' && data[2] == '3')
    { 
       graphic_interface_enabled = 1;
       lcd_print_literal(LCD_SECOND_ROW, "SE: InterfaceGra");

       brgh=get_usart_speed(1200,8000,&sp);
       setup_UART(brgh,sp);
       
       sent_tx_buf = 0; new_tx_buf = 0;
       read_rx_buf = 0; new_rx_buf = 0;
       
       asyn_code_notification_graphic_interface(INITIALA_PIC);
    }
    else
    {
        send_literal("NO");
        lcd_print_literal(LCD_SECOND_ROW, "SE: Con. Putty");
    }
    
    new_rx_buf = 0;
}