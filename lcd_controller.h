/*!*******************************************************************************************
 *  \file       lcd_controller.h
 *  \brief      LCD controller header file.
 *  \details    This file shows the methods that allows use the LCD screen and their documentation.
 *  \author     Marcos Bernal
 *  \author     Antonio Tabernero
 ********************************************************************************************/

#ifndef LCDCONTROLLER_H
#define	LCDCONTROLLER_H

#include "integer_types.h"
#include "configuration_bits.h"

#define LCD_CLEAR 1 //0b00000001
#define LCD_HOME 2   //0b00000010
#define LCD_FIRST_ROW 0
#define LCD_SECOND_ROW 1

#define LCD_CURSOR_OFF 12
#define LCD_UNDERLINE 14
#define LCD_BLINK_ON 15

#define LCD_CURSOR_LEFT 16
#define LCD_CURSOR_RIGHT 20
#define LCD_TURN_OFF 0
#define LCD_TURN_ON 8

#define LCD_SHIFT_RIGHT 28
#define LCD_SHIFT_LEFT  24


#define LCD_ENABLE PORTDbits.RD6
#define LCD_RS PORTDbits.RD4 //Used to diferenciate a data byte from a command byte
#define lcd_data(data) {  PORTD = (PORTD & 0xF0) + (data & 0x0F); }

#define LCD_STROBE  LCD_ENABLE=1; Delay10TCYx(1); LCD_ENABLE=0; 


/*!
 * \brief Enables the conexion used in LCD display (port D) and configure the display controller. 
 */
void lcd_init(void);

/*!
 * \brief Lefts the cursor in the defined position 
 * \param row Represents the row in the LCD screen. Possible values 0 or 1.
 * \param col Represents the col in the LCD screen. Possible values from 0 to 15.
 * NOTE: first row, first col  0x10000000
 *       second row, first col 0x11000000
 */
void move_cursor(short int row, short int col);

/*!
 * \brief Places new text in the LCD display, starting where the cursor has been left
 */
void lcd_text(char *txt);

/*!
 * \brief Places new text in the LCD display, starting in first place of first row or second row.
 */
uint8 lcd_print_literal(uint8 row, const rom char *txt);


#endif	/* LCDCONTROLLER_H */

