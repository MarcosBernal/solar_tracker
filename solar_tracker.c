/*!*******************************************************************************************
 *  \file       solar_tracker.c
 *  \brief      Main program source file.
 *  \details    This file implements the methods used to manage the whole project. 
 *              It also manages the ISR(Interrupt Service Routine).
 *  \author     Marcos Bernal
 *  \author     Antonio Tabernero
 ********************************************************************************************/


#include "integer_types.h"
#include "configuration_bits.h"
#include "lcd_controller.h"
#include "serial_controller.h"
#include "engine_controller.h"
#include "timer_controller.h"
#include "adc_controller.h"

#pragma config OSC=HS
#pragma config PWRT = OFF, BOREN = OFF 
#pragma config WDT = OFF, WDTPS = 128
#pragma config PBADEN = OFF, LVP = OFF


/*! 
 * \brief High priority interruption. 
 */
#pragma interrupt high_ISR
void high_ISR (void) 
{
 if (INT0_flag) encoder_X_interruption();
 if (INT1_flag) encoder_Y_interruption();
}


/*! 
 * \brief Low priority interruption. 
 */
#pragma interruptlow low_ISR
void low_ISR (void) 
{
  if (RX_flag) rx_interruption(); 
  if (TX_flag) tx_interruption();  
  
 if (TMR0_flag)
 {
     prepare_ADC_converter();                        //Prepare the AD_flag
     reset_TMR0();
     TMR0_flag=0;
 }
     
  if (AD_flag)
  {
      if (read_ADC_value() == (NUM_ADC_CHANNELS-1)) //If value != 3 then prepare AD_flag
      {    
        engine_X_spin_interruption();  
        engine_Y_spin_interruption();
        notify_graphic_interface();
      }
      AD_flag=0;
  }
}

// Code @ 0x0008 -> Jump to ISR for Low priority interruption
#pragma code high_vector = 0x0008
  void code_0x0008(void) {_asm goto high_ISR _endasm}
#pragma code

// Code @ 0x0018 -> Jump to ISR for Low priority interruption
#pragma code low_vector = 0x0018
  void code_0x0018 (void){_asm goto low_ISR _endasm}
#pragma code


uint8 current_state = MANUAL_MOVEM_STATE;  //!< Represents the current state in the state machine
uint8 new_state = MANUAL_MOVEM_STATE;      //!< Represents the next state in the state machine. It can be the value of a state or not(NONE_CURRENT_STATE == stays in the previous state)


/*! 
 * \brief Performs the main functionality that has the state machine, processing the three possible orders.
 * \details There are 5 states, but in this code only MANUAL_MOVEM_STATE, SEARCH_LIGHT_STATE, and TRACK__LIGHT_STATE are processed. 
 *          It can be moved on from one state to another whenever an order has been received and processed (new_state changes).
 */
void state_machine(void)
{
    if(new_async_message == 1) //New message
        new_state = manage_received_asyn_messages(); // deal with the new data
    
    switch(current_state)
    {
        case(MANUAL_MOVEM_STATE):
            if(new_state == SEARCH_LIGHT_STATE)
            {
                start_searching_light_spot();
            }
            else if(new_state == TRACK__LIGHT_STATE)
            {
                start_tracking_of_light();
            }
            else if(new_state == CLOSING__PIC_STATE)
            {
                aim_encoder_X = 0;
                aim_encoder_Y = 0;
            }
            else
            {
                if(new_pos_X > 0)
                    aim_encoder_X = (new_pos_X > MAX_POS_ENCODER_X) ? MAX_POS_ENCODER_X: new_pos_X;
                else
                    aim_encoder_X = (new_pos_X < MIN_POS_ENCODER_X) ? MIN_POS_ENCODER_X: new_pos_X;
                
                if(new_pos_Y > 0)
                    aim_encoder_Y = (new_pos_Y > MAX_POS_ENCODER_Y) ? MAX_POS_ENCODER_Y: new_pos_Y;
                else
                    aim_encoder_Y = (new_pos_Y < MIN_POS_ENCODER_Y) ? MIN_POS_ENCODER_Y: new_pos_Y;
 
                new_state = MANUAL_MOVEM_STATE; 
            }

            break;
  
            
        case(SEARCH_LIGHT_STATE):
            if(new_state == TRACK__LIGHT_STATE)
            {
                start_tracking_of_light();
            }
            else if(new_state == MANUAL_MOVEM_STATE)
            {
                stop_searching_light_spot();
            }
            else if(new_state == SEARCH_LIGHT_STATE) //new order
            {
                start_searching_light_spot();
            }
            else if(new_state == CLOSING__PIC_STATE)
            {
                stop_searching_light_spot();
            }
            else
            {
                searching_light_spot();
                new_state = SEARCH_LIGHT_STATE;
            } 
                
            break;
            
            
        case(TRACK__LIGHT_STATE):
            if(new_state == MANUAL_MOVEM_STATE)
            {
                stop_tracking_of_light();
            }
            else if(new_state == SEARCH_LIGHT_STATE)
            {
                stop_tracking_of_light();
                start_searching_light_spot();
            }
            else if(new_state == CLOSING__PIC_STATE)
            {
                stop_tracking_of_light();
            }
            else
            {
                tracking_of_light();
                new_state = TRACK__LIGHT_STATE;
            }
                
            break;

        default:
            break;     
    }
    
    if(current_state != new_state)
        asyn_state_notification_graphic_interface(new_state);
    
    current_state = new_state;
    new_state = NONE_CURRENT_STATE;
    Delay1KTCYx(1);
}

/*! 
 * \brief Main program. 
 * \details It contains the state of initializing and closing that state_machine() does not have.
 */
void main(void) 
{   
    {//INITIALIZING
        
        // Clearing all ports
        TRISA = 0x00; TRISB = 0x00; TRISC = 0x00; TRISD = 0x00; TRISE = 0x00;
        PORTA = 0x00; PORTB = 0x00; PORTC = 0x00; PORTD = 0x00; PORTE = 0x00;


        lcd_init();    
        lcd_print_literal(LCD_FIRST_ROW, "INIT - SOLARTRAC");


        serial_init();
        engines_init();    
        ADC_init();


        asyn_code_notification_graphic_interface(WAITING_TASK);
        asyn_state_notification_graphic_interface(current_state);
    }
    
    while(current_state != CLOSING__PIC_STATE)
        state_machine();
    
    {//CLOSING
        engines_close();
        ADC_close();    
        
        lcd_print_literal(LCD_FIRST_ROW, "END - SOLARTRACK");
        serial_close();    
    }
        
    while(1)Delay10KTCYx(255);
}
